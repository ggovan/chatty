# Chatty

Simple chat application with server and client.

## Prerequisites

- JVM with Gradle
- Node.js with Yarn

## Get up and running

Run the server

```sh
cd server

gradle bootRun
```

Run the client

```sh
cd client
yarn
yarn start
```

## Details

### Client

This is a React app, bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
It runs a server to serve up the page and assets, and acts as reverse proxy through to the server running on port 8080.

The page loads with a "login" screen to get a user name, and then shows the list of messages and the message entry panel.
Users can add new messages, and will receive new messages over websocket.
There is no error handling, retry, reconnection, or fall back for the websocket.

### Server

This is a Spring Boot application using a WebFlux starter and Gradle for the build.
Spring Boot was chosen because of my previous experience with Spring and known feature coverage and support for many different integrations, such as wide range of persistence options.
And WebFlux is used for its nice websocket support.

The persistence is using H2 which runs in process and reads/writes to a file.
The `ChatService` stores the current state of the chat room so that reads don't have to go to the database.
The `ChatService` also publishes new events over websocket to any clients that have connected.
