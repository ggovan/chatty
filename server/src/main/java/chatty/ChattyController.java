package chatty;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletionStage;

@RestController
public class ChattyController {

    private final ChatService store;

    public ChattyController(ChatService store) {
        this.store = store;
    }

    @RequestMapping(path = "/message", method = RequestMethod.GET)
    @ResponseBody
    public CompletionStage<List<ChatEvent>> index() {
        return store.getAllEvents();
    }

    @RequestMapping(path = "/message", method = RequestMethod.PUT)
    public CompletionStage<ChatEvent> put(@RequestBody ChatEvent.WithNoId eventStub) {
        return store.add(eventStub);
    }

}