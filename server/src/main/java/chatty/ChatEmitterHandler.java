package chatty;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Handles websocket connections which have new ChatEvents published on them.
 */
@Component
public class ChatEmitterHandler implements WebSocketHandler {

    private final ChatService store;
    private final ObjectMapper objectMapper;

    public ChatEmitterHandler(ChatService store, ObjectMapper objectMapper) {
        this.store = store;
        this.objectMapper = objectMapper;
    }


    @Override
    public Mono<Void> handle(WebSocketSession session) {
        Flux<ChatEvent> source = Flux.from(store);
        return session.send(source.map(e -> {
            try {
                return objectMapper.writeValueAsString(e);
            } catch (JsonProcessingException e1) {
                throw new RuntimeException("JSON ERROR", e1);
            }
        }).map(session::textMessage));
    }
}
