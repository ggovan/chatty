package chatty;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CompletableFuture;

/**
 * Service that represents a Chat room.
 * <p>
 * Clients are able to get all the messages that have been published, add messages of their own, and subscribe to new messages being added.
 */
@Service
public class ChatService implements Publisher<ChatEvent> {

    /**
     * List of events representing the current state of the chat room.
     * <p>
     * It is initialised with the events from the DB on startup, and then updated with new events afterwards.
     */
    private final CopyOnWriteArrayList<ChatEvent> events;
    private final ChatRepository repo;

    public ChatService(ChatRepository repo) {
        this.repo = repo;
        events = new CopyOnWriteArrayList<>(repo.findAllEvents());
        System.out.println("Loaded " + events.size() + " events");
    }

    public CompletionStage<List<ChatEvent>> getAllEvents() {
        // We should really do a defensive copy to ensure that
        // - callers can't mutate us through this reference
        // - multiple iterations through the list remain consistent
        return CompletableFuture.completedFuture(events);
    }

    /**
     * Adds a new event to the store, and publishes it to any subscribers.
     */
    public CompletionStage<ChatEvent> add(ChatEvent.WithNoId event) {
        // Temporally have an event with a null id so that we can send it to the repo.
        ChatEvent fullEvent = new ChatEvent(null, event.user, event.message, LocalDateTime.now());

        ChatEvent persistedEvent = repo.save(fullEvent);

        events.add(persistedEvent);
        publishToSubscribers(persistedEvent);
        return CompletableFuture.completedFuture(persistedEvent);
    }

    // Also making this a reactive publisher. Can split this out later.

    private CopyOnWriteArrayList<Subscriber<? super ChatEvent>> subscribers = new CopyOnWriteArrayList<>();

    /**
     * Subscribe to new events being added to the store.
     */
    @Override
    public void subscribe(Subscriber<? super ChatEvent> subscriber) {
        subscribers.add(subscriber);
        Subscription subscription = new Subscription() {
            @Override
            public void request(long n) {
                // don't bother with this for now
            }

            @Override
            public void cancel() {
                subscribers.remove(subscriber);
            }
        };
        subscriber.onSubscribe(subscription);
    }

    private void publishToSubscribers(ChatEvent e) {
        for (Subscriber<? super ChatEvent> subscriber : subscribers) {
            subscriber.onNext(e);
        }
    }
}