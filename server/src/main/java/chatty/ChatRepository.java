package chatty;

import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;

public interface ChatRepository extends CrudRepository<ChatEvent, String> {

    default List<ChatEvent> findAllEvents() {
        List<ChatEvent> events = new ArrayList<>();
        findAll().iterator().forEachRemaining(events::add);
        return events;
    }

}
