package chatty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public final class ChatEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public UUID id;
    public String user;
    public String message;
    public LocalDateTime timestamp;

    public ChatEvent() {
    }

    public ChatEvent(UUID id, String user, String message, LocalDateTime timestamp) {
        this.id = id;
        this.user = user;
        this.message = message;
        this.timestamp = timestamp;
    }

    /**
     * A subset of {ChatEvent} that will be received from PUT requests.
     */
    public static class WithNoId {
        public final String user;
        public final String message;

        public WithNoId(String user, String message) {
            this.user = user;
            this.message = message;
        }
    }
}