# Chatty server

Spring Boot server with a Gradle build.

Runs on port `8080` by default. Start with:

```sh
gradle bootRun
```
