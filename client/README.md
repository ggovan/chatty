# Chatty

Simple client to display messages from the server, and put up new messages.

Features:

- "login" screen to set the user name, there's no authentication
- loads all messages stored on the server after login
- can put new messages
- receives a stream of new messages over websocket

Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Running

To get dependencies:

```sh
yarn
```

To run:

```sh
yarn start
```

Reverse proxies api requests to `http://localhost:8080/`, so please ensure that the server is running there.
