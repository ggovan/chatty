import * as React from "react";

import * as api from "./api";
import { MessageList } from "./MessageList";

/**
 * Loads messages from the API on mount and display them in a `MessageList`.
 * Connects over websocket to receive new messages.
 */
export class MessageLoader extends React.Component {
  state = { messages: [] };

  async componentDidMount() {
    this.connectWebsocket();
    const messages = await api.getAllMessages();
    // Messages might already have been set over the websocket.
    // Smarter thing to do would be to prepend the current list with what we've just received
    // and strip out duplicates.
    this.setState({ messages });
  }

  connectWebsocket() {
    // Hardcoded this, should probably change it.
    const socket = new WebSocket("ws://localhost:3000/ws");
    socket.onmessage = e =>
      this.setState({
        messages: [
          ...this.state.messages,
          { ...JSON.parse(e.data), isNew: true }
        ]
      });
    // Should really close the websocket when the component is unmounted,
    // but that will only happen on page refresh just now.
  }

  render() {
    return <MessageList {...this.props} messages={this.state.messages} />;
  }
}
