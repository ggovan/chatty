import React from "react";

import { Message } from "./Message";

import "./MessageList.css";

/**
 * Displays a list of messages.
 */
export const MessageList = ({ messages, currentUser }) => (
  <section className="MessageList">
    {messages.map(message => (
      <Message
        {...message}
        fromSelf={currentUser === message.user}
        key={message.id}
      />
    ))}
  </section>
);
