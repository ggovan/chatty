import * as React from "react";

/**
 * Landing component to get the user's name.
 */
export class Login extends React.Component {
  state = {
    name: ""
  };

  get isInputValid() {
    return this.state.name.trim().length > 0;
  }

  render() {
    return (
      <section>
        <form
          onSubmit={e => {
            e.preventDefault();
            if (this.isInputValid) {
              this.props.onLogin(this.state.name.trim());
            }
          }}
        >
          <label>
            Your name:
            <input
              type="text"
              onChange={e => this.setState({ name: e.target.value })}
              value={this.state.name}
              autoFocus={true}
            />
          </label>
        </form>
      </section>
    );
  }
}
