/**
 * Get all the messages from the API.
 */
export const getAllMessages = async () => {
  const res = await fetch("/message", {
    headers: {
      Accept: "application/json"
    }
  });

  if (res.status === 200) {
    return await res.json();
  } else {
    throw new Error(`Put request failed: ${res.statusText}`);
  }
};

/**
 * Send a message to the API.
 * @param {string} user
 * @param {string} message
 */
export const putMessage = async (user, message) => {
  const res = await fetch("/message", {
    method: "PUT",
    body: JSON.stringify({ user, message }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  });

  if (res.status === 200) {
    return await res.json();
  } else {
    throw new Error(`Put request failed: ${res.statusText}`);
  }
};
