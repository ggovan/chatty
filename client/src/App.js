import * as React from "react";

import { MessageLoader } from "./MessageLoader";
import { MessageEntry } from "./MessageEntry";
import { Login } from "./Login";
import * as api from "./api";

import "./App.css";

class App extends React.Component {
  state = {
    name: null
  };

  get loggedIn() {
    return this.state.name !== null;
  }

  onLogin = name => this.setState({ name });

  render() {
    return (
      <div className="App">
        <header className="App-header">Chatty</header>
        {this.loggedIn ? (
          <>
            <MessageLoader currentUser={this.state.name} />
            <MessageEntry
              submitMessage={message =>
                api.putMessage(this.state.name, message)
              }
            />
          </>
        ) : (
          <Login onLogin={this.onLogin} />
        )}
      </div>
    );
  }
}

export default App;
