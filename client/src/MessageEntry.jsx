import * as React from "react";

import "./MessageEntry.css";

/**
 * Text Area entry with submit button.
 */
export class MessageEntry extends React.Component {
  state = {
    message: ""
  };

  submit = () => {
    this.props.submitMessage(this.state.message);
    this.setState({ message: "" });
  };

  render() {
    return (
      <section className="MessageEntry">
        <form
          onSubmit={e => {
            e.preventDefault();
            this.submit();
          }}
        >
          <textarea
            autoFocus={true}
            onChange={e => this.setState({ message: e.target.value })}
            value={this.state.message}
            onKeyDown={e => {
              if (e.key === "Enter" && !e.shiftKey) {
                this.submit();
                e.preventDefault();
              }
            }}
          />
          <button>Send</button>
        </form>
      </section>
    );
  }
}
