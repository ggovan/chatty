import React from "react";
import { DateTime } from "luxon";

import "./Message.css";

const formatDate = dateString =>
  DateTime.fromISO(dateString).toFormat("dd LLL yyyy HH:ss");

/**
 * Displays a single message.
 * Uses special formatting for new and messages from self.
 */
export const Message = ({ user, message, timestamp, isNew, fromSelf }) => (
  <div
    className={`Message ${isNew ? "isNew" : ""} ${fromSelf ? "fromSelf" : ""}`}
  >
    <div className="Message-user">{fromSelf ? "You" : user}</div>
    <div className="Message-message">{message}</div>
    <div className="Message-timestamp">{formatDate(timestamp)}</div>
  </div>
);
