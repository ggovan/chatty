const proxy = require("http-proxy-middleware");

module.exports = app => {
  // Proxy settings in package.json don't support websockets, 95% sure they used to!
  app.use(proxy("/ws", { target: "http://localhost:8080", ws: true }));
};
